# Machine Zoo

Database of Finite State Machines
* Acceptors (FSA)
* Transducers (FST)
* Dense machines
* Random machines
* n-gram Language Models

## Code

* Script for creating random machines
* Script for crearing dense machines from neural network output
* Script for creating machines from n-gram language models
  