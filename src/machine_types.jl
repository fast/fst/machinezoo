function random_fst(S::Type{<:Semirings.Semiring},
	nstates,
	nisyms,
	arc_density;
	narcs = nothing,
	unweigthed = false,
	acyclic = false,
	label_offset = 1,
	nosyms = nothing,
	acceptor = false,
	seed = 1234)

	if nosyms === nothing
		nosyms = nisyms
	end
	if acceptor
		_nsyms = nisyms
	else
		_nsyms = nisyms * nosyms
	end

	if acyclic && isnothing(narcs)
		narcs = floor(Int, arc_density * _nsyms * nstates * (nstates - 1) / 2)
    elseif !acyclic && isnothing(narcs)
		narcs = floor(Int, arc_density * _nsyms * nstates^2)
	end

	println("nstates: ", nstates, " nisyms: ", nisyms, " narcs: ", narcs, " unweigthed: ", unweigthed, " acyclic: ", acyclic, " nosyms: ", nosyms, " acceptor: ", acceptor, " seed: ", seed)

	warcs = random_arcs(S, nstates, nisyms, narcs; unweigthed = unweigthed, acyclic = acyclic, nosyms = nosyms,
		label_offset = label_offset, acceptor = acceptor, seed = seed)

	# warcs = random_arcs_loop(S, nstates, nisyms, narcs; unweigthed = unweigthed, acyclic = acyclic, nosyms = nosyms,
	# 	label_offset = label_offset, acceptor = acceptor, seed = seed)

	if unweigthed
		wfinal = one(S)
	else
		wfinal = S(rand())
	end
	SparseTensorFSM{S,2}(warcs, [1 => one(S)], [nstates => wfinal])
end

function random_arcs_loop(S::Type{<:Semirings.Semiring}, nstates, nisyms, narcs; unweigthed = false, acyclic = false,
	label_offset = 1, nosyms = nothing, acceptor = false, seed = 1234)

	if nosyms === nothing
		nosyms = nisyms
	end

	Random.seed!(seed)
	arcs = []
	warcs = []

	if acceptor
		total_arcs = nstates^2 * (nisyms)
		if narcs > total_arcs
			throw(ArgumentError("Number of arcs must be less than nstates^2 nsyms, $(narcs) > $(total_arcs)"))
		end
	else
		total_arcs = nstates^2 * (nisyms) * (nosyms)
		if narcs > total_arcs
			throw(ArgumentError("Number of arcs must be less than nstates^2 nisyms nosyms, $(narcs) > $(total_arcs) "))
		end
	end

	keys = Dict()

	for n in 1:narcs
		notok = true
		while notok

			if acceptor
				il = rand(label_offset:nisyms+label_offset-1)
				ol = il
			else
				il, ol = rand(label_offset:nisyms+label_offset-1), rand(label_offset:nisyms+label_offset-1)
			end

			if acyclic
				ss = rand(1:nstates-1)
				ds = rand(ss+1:nstates)
			else
				ss, ds = rand(1:nstates), rand(1:nstates)
			end

			notok = false

			arc = (il, ol, ss, ds)

			if haskey(keys, arc)
				notok = true
			end

			if !notok
				keys[arc] = true
				if unweigthed
					weight = one(S)
				else
					weight = S(rand())
				end
				warc = TF.Arc(arc[3], arc[4], weight, arc[1], arc[2])
				push!(arcs, arc)
				push!(warcs, warc)
			end
		end
	end
	p = sortperm(arcs)
	warcs[p]
end

function random_arcs(S::Type{<:TensorFSTs.Semiring}, nstates, nisyms, narcs; unweigthed=false, acyclic=false,
                        label_offset=1, nosyms=nothing, acceptor=false, seed=1234)

	if nosyms === nothing
		nosyms = nisyms
	end	

	Random.seed!(seed)
	arcs = []
	warcs = []

	if acceptor
		total_arcs = nstates^2*nisyms
		if narcs > total_arcs
			throw(ArgumentError("Number of arcs must be less than nstates^2 nsyms, $(narcs) > $(total_arcs)"))
		end
		cis = CartesianIndices((nstates,nstates,nisyms))
	else
		total_arcs = nstates^2*nisyms*nosyms
		if narcs > total_arcs
			throw(ArgumentError("Number of arcs must be less than nstates^2 nisyms nosyms, $(narcs) > $(total_arcs) "))
		end
		cis = CartesianIndices((nstates,nstates,nisyms,nosyms))
	end


	if acyclic
		new_cis = []
		for c in cis
			if c[1]<c[2]
				push!(new_cis,c)
			end
		end
		cis = new_cis

		if acceptor
			total_arcs = floor(Int,(nstates^2-nstates)/2*(nisyms-label_offset+1))
			if narcs > total_arcs
				throw(ArgumentError("Number of arcs must be less than nstates(nstates-1)/2 nsyms, $(narcs) > $(total_arcs)"))
			end
		else
			total_arcs = floor(Int,(nstates^2-nstates)/2*(nisyms-label_offset+1)*(nosyms-label_offset+1))
			if narcs > total_arcs
				throw(ArgumentError("Number of arcs must be less than  nstates(nstates-1)/2 nisyms nosyms, $(narcs) > $(total_arcs) "))
			end
		end
	end

	ixs = sample(1:total_arcs, narcs; replace=false)
	cis = cis[ixs]

	for c in cis
		if acceptor
			ss,ds,il = Tuple(c)
			ol = il
		else
			ss,ds,il,ol = Tuple(c)
		end

		il = il + label_offset - 1
		ol = ol + label_offset - 1 
		
		arc = (il, ol, ss, ds)
		if unweigthed
			weight = one(S)
		else
			weight = S(rand())
		end

		warc = TF.Arc(arc[3], arc[4], weight, arc[1], arc[2])
		push!(arcs, arc)
		push!(warcs, warc)
	end
	p = sortperm(arcs)
	warcs[p]
end


function dense_random_fst(S::Type{<:Semirings.Semiring}, n_states, n_symbols; seed = 1234)
	Random.seed!(seed)
	symbs_ids = 1:n_symbols
	y = S.(rand(n_symbols, n_states))
	TF.LinearFSTs.linearfst(y, symbs_ids)
end

"""
	complete_acyclic_fst(S, nstates, nisyms; unweigthed = false, label_offset = 1, seed = 1234)

Create a complete acyclic FST acceptor with `nstates` states and `nisyms` input/output symbols.

"""

function complete_acyclic_fst(S::Type{<:Semirings.Semiring}, nstates, nisyms;
	unweigthed = false, label_offset = 1, seed = 1234)

	Random.seed!(seed)
	narcs = Int(nstates * (nstates - 1)/2*nisyms)
	warcs = Array{TF.Arc}(undef, narcs)
	c = 0
	for i in 1:nstates
		for j in i+1:nstates
			for k in label_offset:nisyms+label_offset-1
				if unweigthed
					weight = one(S)
				else
					weight = S(rand())
				end
				warc = TF.Arc(i, j, weight, k, k)
				c+=1
				warcs[c] =  warc
			end
		end
	end
	SparseTensorFSM{S,2}(warcs, [1 => one(S)], [nstates => one(S)])
end

"""
	topology_fst(S, topo, initweights, finalweights, symbols, mapping)

Create a FST composed of a set of smaller FSTs. Each symbol share the same FST
topology specified with `topo = [(src, dest, weight), ...]`. `S` is a
`Semiring` of output FST, `symbols` is a list of symbol IDs.
"""
function topology_fst(
	S,
	topo,
	initweights,
	finalweights,
	symbols,
)

	arcs, init, final = [], [], []

	initdict = Dict(initweights...)
	finaldict = Dict(finalweights...)

	base_state = 1
	offset = 1
	for (j, symbol) in enumerate(symbols)

		for topo_arc in topo

			src, dest, weight = offset + topo_arc[1], offset + topo_arc[2], topo_arc[3]

			arc = Arc(
				src = src,
				isym = symbol,
				osym = 0,
				dest = dest,
				weight = S(weight),
			)

			# Initialization arcs
			if topo_arc[1] in keys(initdict)
				init_arc = Arc(
					src = base_state,
					isym = 0,
					osym = 0,
					dest = src,
					weight = S(initdict[topo_arc[1]]),
				)
				push!(arcs, init_arc)
			end
			if topo_arc[2] in keys(initdict)
				init_arc = Arc(
					src = base_state,
					isym = 0,
					osym = 0,
					dest = dest,
					weight = S(initdict[topo_arc[2]]),
				)
				push!(arcs, init_arc)
			end

			# Final arcs
			if topo_arc[1] in keys(finaldict)
				final_arc = Arc(
					src = src,
					isym = 0,
					osym = symbol,
					dest = base_state,
					weight = S(finaldict[topo_arc[1]]),
				)
				push!(arcs, final_arc)
			end
			if topo_arc[2] in keys(finaldict)
				final_arc = Arc(
					src = dest,
					isym = 0,
					osym = symbol,
					dest = base_state,
					weight = S(finaldict[topo_arc[2]]),
				)
				push!(arcs, final_arc)
			end

			push!(arcs, arc)
		end
		offset = offset + topo[end][2]
	end

	push!(init, base_state => one(S))
	push!(final, base_state => one(S))
	SparseTensorFSM{S,2}(arcs, init, final)
end
