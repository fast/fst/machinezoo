module MachineZoo

using Semirings
using DataFrames
import IterTools.product
using ProgressBars
using CSV
using Random
using StatsBase
using TensorFSTs
using JSON
using OpenFst

include("../../TensorFSTs.jl/lib/OpenFstConvert.jl")
Random.seed!(123456)

TF = TensorFSTs
SR = Semirings

export

    # Machine types
    random_arcs,
    random_fst,
    dense_random_fst,
    complete_acyclic_fst,
    topology_fst,

    # Utils
    call_generator,
    fstinfo,

    #Charlm
    txt2charlm


include("utils.jl")
include("machine_types.jl")

end