using ArgParse
using MachineZoo
using CSV
using DataFrames
using Glob

function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table s begin
        "--dir", "-d"
        help = "where to look for files"
        arg_type = String
        required = true
    end
    parse_args(ARGS, s)
end

args = parse_commandline()

csv_output = joinpath(args["dir"],"fstinfo.csv")

if !isfile(csv_output)
    files = glob(joinpath(args["dir"],"*.fst"))
    println(files)
    if length(files) == 0
        error("No files found in $(args["dir"])")
    else
        df = vcat(fstinfo.(files)..., cols = :union)
        df = unstack(df, :file, :prop, :val)
        CSV.write(csv_output, df)
    end
end